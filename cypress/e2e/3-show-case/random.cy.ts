describe('', function () {
    beforeEach(function () {
        cy.visit("#/show-case/random")
    });
    it.skip('should be parse -fail  ', function () {
        cy.get("#random-number")
            .invoke("text")
            .then(parseFloat)
            .should("be.gte", 1)
            .should("be.lte", 10)
    });
    it('should be parse -fail  ', function () {
        cy.get("#random-number")
            .should(($div) => {
                const n = parseFloat($div.text());
                expect(n).to.be.gte(1).and.to.be.lte(10)
            })
    });
});