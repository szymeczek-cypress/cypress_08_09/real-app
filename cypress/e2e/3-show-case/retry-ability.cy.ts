describe('', function () {
    beforeEach(function () {
        cy.visit("#/show-case/retry-ability")
    });
    it('should ', function () {
        cy.getByName("task-input").type("Zadanie 2{enter}")
        cy.getByName("task-input").type("Zadanie 3{enter}")
        cy.getBySel("tasks-list").find("li").should("have.length", 3)
    });

    it.only('should check lent with clock ', function () {
        cy.clock()
        cy.getByName("task-input").type("Zadanie 2{enter}")
        cy.tick(2000)
        cy.getByName("task-input").type("Zadanie 3{enter}")
        cy.tick(2000)
        cy.getBySel("tasks-list").find("li").should("have.length", 3)
    });
});