describe('first test', function () {
    beforeEach(function () {
        cy.visit("/")
    });
    it('should open pulpit and move to show case page', function () {
        // cy.contains("Pulpit").click()
        // cy.url().should("eq", "http://localhost:3000/#/show-case")

        cy.contains("Pulpit")
            .should("have.attr", "href", "#/show-case")
    });
});