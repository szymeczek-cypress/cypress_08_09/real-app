export function validateInputExpectedMessage(inputName: string, expectedValidationMesage: string) {
    cy.getByName(inputName)
        .parents(".mantine-TextInput-root")
        .find(".mantine-TextInput-error")
        .should("exist")
        .should("have.text", expectedValidationMesage)
}

export function validateRequiredOnFocus(inputName: string, expectedValidationMesage: string) {
    cy.getByName(inputName).focus().blur()
    validateInputExpectedMessage(inputName, expectedValidationMesage);
}

export function validateSelectExpectedMessage(inputName: string, expectedValidationMessage: string) {
    cy.getByName(inputName)
        .parents('.mantine-Select-root')
        .find('.mantine-Select-error')
        .should('exist')
        .should('have.text', expectedValidationMessage);
}

export function validateRequiredOnSelect(inputName: string, expectedValidationMessage: string) {
    cy.getByName(inputName).as('input');

    cy.get('@input').parent().click();
    cy.get('@input').parents('form').click();
    validateSelectExpectedMessage(inputName,expectedValidationMessage);
}