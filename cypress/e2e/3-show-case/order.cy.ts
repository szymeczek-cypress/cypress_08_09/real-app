import {
    validateInputExpectedMessage,
    validateRequiredOnFocus,
    validateRequiredOnSelect,
    validateSelectExpectedMessage
} from "./mantine-utils";

describe('', function () {

    beforeEach(function () {
        cy.visit("/#/products/49963fe1-5339-4f87-926e-82c890da27c4/buy-and-pay")
    });
    it('should ', function () {
        cy.intercept("POST", "/api/create-order").as("createOrder")

        cy.getByName("taxId").type("1171340996")
        cy.getByName("address.streetNumber").type("12")
        cy.getByName("address.postCode").type("40-555")
        cy.getByName("address.place").type("Kraków")

        cy.getByName("address.country").parent().click()
        cy.get(".mantine-Select-item").contains("Polska").click()

        cy.getByName("delivery-form").submit()
        cy.contains("Dostawa")
            .should("have.attr", "data-completed", "true")
        cy.getBySel("payment-button-credit-card").click()
        cy.wait("@createOrder", {timeout: 6000})
        cy.get("@createOrder").its("response.statusCode").should("eq", 200)
        cy.contains("Płatność")
            .should("have.attr", "data-completed", "true")

        cy.get("@createOrder").its("request.body")
            .should("deep.include", {
                "address": {
                    "type": "STREET",
                    "street": "",
                    "streetNumber": "12",
                    "flatNumber": "",
                    "postCode": "40-555",
                    "place": "Kraków",
                    "country": "PL"
                },
                "taxId": "1171340996"
            })
    });
    it('should check focus require fields in order', function () {
        validateRequiredOnFocus("taxId", "Numer nip jest wymagany");
        validateRequiredOnFocus("address.postCode", "Kod jest wymagany");
        validateRequiredOnFocus("address.place", "Miejscowość jest wymagana");
        validateRequiredOnFocus("address.streetNumber", "Numer jest wymagany");
        validateRequiredOnSelect("address.country", "Wybierz kraj")
    });

    it('should check require fields in order', function () {
        cy.getByName("delivery-form").submit()
        validateInputExpectedMessage("taxId", "Numer nip jest wymagany");
        validateInputExpectedMessage("address.postCode", "Kod jest wymagany");
        validateInputExpectedMessage("address.place", "Miejscowość jest wymagana");
        validateInputExpectedMessage("address.streetNumber", "Numer jest wymagany");
        validateSelectExpectedMessage("address.country", "Wybierz kraj")
    });
    it.only('should external init ', function () {
        cy.window()
            .its("orderForm")
            .invoke("setRecoilState", {
                address: {
                    type: "STREET",
                    street: "Mickiewicza",
                    streetNumber: "",
                    flatNumber: "50",
                    postCode: "40-234",
                    place: "Kraków",
                    country: ""
                },
                addressDelivery: {
                    type: "STREET",
                    street: "Woronicza",
                    streetNumber: "14",
                    flatNumber: "4",
                    postCode: "",
                    place: "",
                    country: ""
                },
                useAddressAsDelivery: false,
                phone: "",
                fastDelivery: false,
                taxFree: true,
                individual: false,
                taxId: "",
            })
    });
});


