describe('', function () {
    beforeEach(function () {
        cy.visit("#/show-case/simple-form")
    });
    it('should test happy path auth form', function () {
        // cy.get('[data-test="auth-email"]').type("ziutek@example.com")
        // cy.get('[data-test="auth-password"]').type("password")

        // cy.get('[data-test="auth-submit"]').click()
        cy.getBySel('auth-email').type("ziutek@example.com")
        cy.getBySel('auth-password').type("password")
        cy.getBySel("auth-submit").click();
    });
});