describe('', function () {
    beforeEach(function () {
        cy.visit("/#/products")
    });
    it('should ', function () {
        cy.intercept("GET", "/api/products", {fixture:"products.json"}).as("getProducts")
    });

    it('should present empty message products', function () {
        cy.intercept("GET", "/api/products", [] ).as("getProducts")
        cy.wait("@getProducts")
        cy.getBySel("message-empty").should("have.text", "Lista produktów jest pusta")
    });
});