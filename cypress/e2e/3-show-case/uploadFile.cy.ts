const FILE_UPLOAD_INPUT_SELECTOR = "[data-test=file-upload] input[type=file]";

describe('Upload file', function () {
    beforeEach(function () {
        cy.visit("#/show-case/dropzone")
    });
    it('should upload file', function () {
        // cy.getBySel("file-upload").find("input[type=file]")
        //     .selectFile("cypress/fixtures/sample.pdf", {force: true})

        cy.get(FILE_UPLOAD_INPUT_SELECTOR)
            .selectFile("cypress/fixtures/sample.pdf", {force: true})
        cy.getBySel("file-result").should("contain.text", "sample.pdf")
    });
});