/// <reference types="cypress" />
// ***********************************************
// This example commands.ts shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//
// declare global {
//   namespace Cypress {
//     interface Chainable {
//       login(email: string, password: string): Chainable<void>
//       drag(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       dismiss(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       visit(originalFn: CommandOriginalFn, url: string, options: Partial<VisitOptions>): Chainable<Element>
//     }
//   }
// }

Cypress.Commands.add("getBySel", (selector, ...args) => {
    return cy.get(`[data-test='${selector}']`, ...args);
});
Cypress.Commands.add("getByName", (selector, ...args) => {
    return cy.get(`[name='${selector}']`, ...args);
    // return cy.get("[name='"+selector+"']", ...args);
});

Cypress.Commands.add("dragAndDrop", (sourceSelector, targetSelector, ...args) => {
    cy.get(targetSelector).then($target => {
        let boundingClientRect = $target[0].getBoundingClientRect();
        cy.get(sourceSelector).first().then($source => {
            let sourceoundingClientRect = $source[0].getBoundingClientRect();
            cy.wrap($source).trigger("mousedown", {
                button: 0,
                clientX: sourceoundingClientRect.x,
                clientY: sourceoundingClientRect.y,
                force: true
            })
                .trigger("mousemove", {
                    button: 0,
                    clientX: 10 + sourceoundingClientRect.x,
                    clientY: 10 + sourceoundingClientRect.y,
                    force: true
                })

            cy.get("body").trigger("mousemove", {
                button: 0,
                clientX: boundingClientRect.x,
                clientY: boundingClientRect.y,
                force: true
            }).trigger("mouseup")

        })
    })
});