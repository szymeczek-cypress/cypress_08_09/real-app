/// <reference types="cypress" />

declare namespace Cypress {

    interface Chainable {
        getBySel(dataTestAttribute: string, args?: any): Chainable<JQuery<HTMLElement>>;
        getByName(dataTestAttribute: string, args?: any): Chainable<JQuery<HTMLElement>>;
        dragAndDrop(sourceSelector: string, targetSelector: string): Chainable<JQuery<HTMLElement>>;

    }
}
