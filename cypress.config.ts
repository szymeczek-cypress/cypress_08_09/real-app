import { defineConfig } from "cypress";

export default defineConfig({
  e2e: {
    baseUrl: "http://localhost:3000",
    experimentalStudio: false,
    viewportWidth: 1280,
    viewportHeight: 1024,
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
