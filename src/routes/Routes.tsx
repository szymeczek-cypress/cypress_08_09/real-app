import {useRoutes} from "react-router-dom";
import {MainLayout} from "../layout/MainLayout";
import {Welcome} from "../pages/Welcome";
import Products from "../pages/Products";
import {BuyAndPay} from "../pages/BuyAndPay";
import {ShowCase} from "../pages/ShowCase";
import {DragAndDrop} from "../pages/DragAndDrop";
import {RandomPage} from "../pages/RandomPage";
import {RetryAbility} from "../pages/RetryAbility";
import {SwitchesPage} from "../pages/SwitchesPage";
import {OverlayDemo} from "../pages/ShowCase/OverlayDemo";
import { DropzoneButton } from "../pages/ShowCase/FileUpload";
import {AuthenticationForm} from "../pages/ShowCase/AuthenticationForm";

export const Routes = () => {
    return useRoutes([
        {
            path: '/',
            element: <MainLayout/>,
            children: [
                {
                    path: '/',
                    element: <Welcome/>
                },
                {
                    path: '/show-case',
                    element: <ShowCase/>
                },
                {
                    path: '/show-case/overlay',
                    element: <OverlayDemo/>
                },
                {
                    path: '/show-case/simple-form',
                    element: <AuthenticationForm/>
                },
                {
                    path: '/show-case/dropzone',
                    element: <DropzoneButton/>
                },
                {
                    path: '/show-case/drag-and-drop',
                    element: <DragAndDrop/>
                },
                {
                    path: '/show-case/random',
                    element: <RandomPage/>
                },
                {
                    path: '/show-case/switches',
                    element: <SwitchesPage/>
                },
                {
                    path: '/show-case/retry-ability',
                    element: <RetryAbility/>
                },
                {
                    path: '/products',
                    element: <Products/>
                },
                {
                    path: '/products/:productId/buy-and-pay',
                    element: <BuyAndPay/>
                }
            ]
        }])
};
