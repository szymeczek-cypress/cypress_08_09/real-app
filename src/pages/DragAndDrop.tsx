import React from "react";
import {DndListHandle} from "./ShowCase/DndListHandle";

export const DragAndDrop = () => {
    return <DndListHandle data={[
        {
            "position": 6,
            "mass": 12.011,
            "symbol": "C",
            "name": "Carbon"
        },
        {
            "position": 7,
            "mass": 14.007,
            "symbol": "N",
            "name": "Nitrogen"
        },
        {
            "position": 39,
            "mass": 88.906,
            "symbol": "Y",
            "name": "Yttrium"
        },
        {
            "position": 56,
            "mass": 137.33,
            "symbol": "Ba",
            "name": "Barium"
        },
        {
            "position": 58,
            "mass": 140.12,
            "symbol": "Ce",
            "name": "Cerium"
        },
        {
            "position": 58,
            "mass": 140.12,
            "symbol": "A",
            "name": "A"
        }
        , {
            "position": 58,
            "mass": 140.12,
            "symbol": "B",
            "name": "B"
        }
        , {
            "position": 58,
            "mass": 140.12,
            "symbol": "D",
            "name": "D"
        }
    ]}/>
};
