import React, {useEffect, useState} from 'react';
import {ProductCard} from "./Products/ProductCard";
import {Container, Grid, Skeleton, Box} from "@mantine/core";
import {BuyAndPayModal} from "./Products/BuyAndPayModal";
import axios from "axios";

type Product = {
    id: string,
    image: string, title: string,
    category: string;
    description: string;
    badges: {
        emoji: string;
        label: string;
    }[]
}
const Products = () => {
    const [products, setProducts] = useState<Product[]>([]);
    const [loading, setLoading] = useState<boolean>(false);
    useEffect(() => {
        setLoading(true);
        axios.get("/api/products").then(r => {
            setProducts(r.data);
            setLoading(false)
        })
    }, [])
    return (
        <Container>
            <BuyAndPayModal/>
            <Skeleton visible={loading} height={8} mt={6} radius="xl">
                <Grid align={"top"} data-test="product-lists">
                    {products?.map(item =>
                        <Grid.Col key={item.id} md={4} xs={6}
                                  style={{minHeight: 600}}><ProductCard {...item} /></Grid.Col>
                    )}
                </Grid>
                {
                    products && products.length === 0  && <Box data-test="message-empty">Lista produktów jest pusta</Box>
                }
            </Skeleton>
        </Container>
    )
};

export default Products;