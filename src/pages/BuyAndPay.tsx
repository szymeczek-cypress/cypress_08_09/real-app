import React from 'react';
import {Container, Space, Title} from "@mantine/core";
import {BuyAndPayStepper} from "./Products/BuyAndPayStepper";
import {useParams} from "react-router-dom";

export const BuyAndPay = () => {
    const {productId} = useParams();

    return (
        <Container data-test={"content"}>
            <Title order={3} data-test={"header"}>
                Kupuję i płacę
            </Title>
            <Space h="md" />
            {productId &&
                <BuyAndPayStepper productId={productId}/>
            }
        </Container>
    )

};
