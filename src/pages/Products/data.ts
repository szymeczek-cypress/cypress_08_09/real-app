export const products = [
    {
        id: "49963fe1-5339-4f87-926e-82c890da27c4",
        "image": "https://via.placeholder.com/250",
        "title": "Galaxy-eko groszek",
        "category": "Opał",
        "description": "Międzygwiezdny galaktyczny eko groszek. Gwarantowana dostawa przed sezonem grzewczym 2022",
        "badges": [
            {
                "emoji": "☀️",
                "label": "Darmowa dostawa"
            },
            {
                "emoji": "🦓",
                "label": "Nowy"
            },
            {
                "emoji": "🌊",
                "label": "Nie zużywa zasobów Ziemi"
            },
            {
                "emoji": "🌲",
                "label": "Produkt naturalny"
            },
            {
                "emoji": "🤽",
                "label": "Długo dojrzewający"
            }
        ]
    },
    {
        id: "bcaa0feb-6d16-4041-a721-a01f47cf9801",
        "image": "https://via.placeholder.com/250",
        "title": "Galaxy kostka",
        "category": "Opał",
        "description": "Międzygwiezdny galaktyczny eko groszek. Gwarantowana dostawa przed sezonem grzewczym 2022",
        "badges": [
            {
                "emoji": "☀️",
                "label": "Darmowa dostawa"
            }
        ]
    }

]