import {Anchor, Card, createStyles, Group, SimpleGrid, Text, UnstyledButton} from '@mantine/core';
import {BuildingBank, CreditCard, Repeat} from "tabler-icons-react";

const mockdata = [
    {title: 'Karta kredytowa', icon: CreditCard, color: 'violet', id: "credit-card"},
    {title: 'Pay by link', icon: BuildingBank, color: 'indigo', id: "pay-by-link"},
    {title: 'Zwykły przelew', icon: Repeat, color: 'blue', id: "transfer"},
];

const useStyles = createStyles((theme) => ({
    card: {
        backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.colors.gray[0],
    },

    title: {
        fontFamily: `Greycliff CF, ${theme.fontFamily}`,
        fontWeight: 700,
    },

    item: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        borderRadius: theme.radius.md,
        height: 90,
        backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white,
        transition: 'box-shadow 150ms ease, transform 100ms ease',

        '&:hover': {
            boxShadow: `${theme.shadows.md} !important`,
            transform: 'scale(1.05)',
        },
    },
}));
type Props = {
    onSuccess: () => void;
}

export function PaymentsGrid({onSuccess}: Props) {
    const {classes, theme} = useStyles();

    const items = mockdata.map((item) => (
        <UnstyledButton onClick={onSuccess} key={item.title} className={classes.item} data-test={`payment-button-${item.id}`}>
            <item.icon color={theme.colors[item.color][6]} size={32}/>
            <Text size="xs" mt={7}>
                {item.title}
            </Text>
        </UnstyledButton>
    ));

    return (
        <Card withBorder radius="md" className={classes.card}>
            <Group position="apart">
                <Text data-test="sub-header" className={classes.title}>Metody płatności</Text>
                <Anchor size="xs" color="dimmed" sx={{lineHeight: 1}}>
                    + 21 other services
                </Anchor>
            </Group>
            <SimpleGrid cols={3} mt="md">
                {items}
            </SimpleGrid>
        </Card>
    );
}