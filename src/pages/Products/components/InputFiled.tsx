import {useField} from "formik";
import {Input} from "@mantine/core";
import React from "react";

export function InputFiled({name}: { name: string }) {
    const [{value}, _, {setValue, setTouched}] = useField(name)
    return (
        <Input
            value={value}
            onFocus={() => setTouched(true, true)}
            onChange={(v: any) => setValue(v.target.value)}
        />
    )
}