import React from 'react';
import {Grid,  Text} from "@mantine/core";
import {Field} from "formik";
import {SegmentedControlField} from "./SegmentedControlField";
import {SelectField} from "./SelectField";
import {TextInputField} from "./TextInputField";

type AddressFormProps = {
    namespace?: string,
    title?: string,

}

const AddressForm = ({namespace, title}: AddressFormProps) => {
    const withNamespace = (fieldName: string) => {
        return namespace ? `${namespace}.${fieldName}` : fieldName
    }
    return (
        <div data-test="address-sub-form">
            <Grid>
                {title && <Text size="lg">{title}</Text>}
                <Grid.Col xs={12}>
                    <Field name={withNamespace("type")}
                           as={SegmentedControlField} data={[

                        {label: 'Ulica', value: 'STREET'},
                        {label: 'Aleja', value: 'ALLEY'},
                        {label: 'Osiedle', value: 'SUBDIVISION'},
                    ]}/>
                </Grid.Col>

                <Grid.Col xs={6}>
                    <Field
                        name={withNamespace("street")}
                        label="Ulica"
                        as={TextInputField}/>
                </Grid.Col>
                <Grid.Col xs={2}>
                    <Field
                        required
                        name={withNamespace("streetNumber")}
                        label="Numer domu"
                        as={TextInputField}/>
                </Grid.Col>
                <Grid.Col xs={2}>
                    <Field
                        name={withNamespace("flatNumber")}
                        label="Numer"
                        as={TextInputField}/>
                </Grid.Col>
                <Grid.Col xs={4}>
                    <Field
                        required
                        name={withNamespace("postCode")}
                        label="Kod pocztowy"
                        as={TextInputField}/>
                </Grid.Col>
                <Grid.Col xs={4}>
                    <Field
                        required
                        name={withNamespace("place")}
                        label="Miejscowość"
                        as={TextInputField}/>
                </Grid.Col>
                <Grid.Col xs={4}>
                    <Field
                        name={withNamespace("country")}
                        as={SelectField}
                        data={[
                            {value: 'PL', label: 'Polska'},
                            {value: 'Cz', label: 'Czechy'},
                            {value: 'UK', label: 'Wielka Brytania '},
                        ]}
                        label={"Kraj"}>
                    </Field>
                </Grid.Col>
            </Grid>
        </div>
    );
};

export default AddressForm;
