import React from 'react';
import {Field, Form, Formik, FormikHelpers} from "formik";
import {Button, Checkbox, Grid, Group, Popover, Text} from "@mantine/core";
import {JsonPrint} from "../../../utils/JsonPrint";
import AddressForm from "./AddressForm";
import {CheckboxCard} from "./CheckboxCardProps";
import * as Yup from 'yup';
import {TextInputField} from "./TextInputField";
import {useRecoilState} from "recoil";
import {deliveryFormAtom} from "../../../state/context";
import {useDisclosure} from "@mantine/hooks";

type Address = {
    type: "STREET" | "ALLEY" | "SUBDIVISION"
    street: string,
    streetNumber: string,
    flatNumber: string,
    postCode: string,
    place: string,
    country: string
}
export type OrderProps = {
    address: Address,
    useAddressAsDelivery: boolean,
    addressDelivery: Address,
    phone: string,
    fastDelivery: boolean,
    taxFree: boolean,
    individual: boolean,
    taxId: string

}

function defaultAddressSchema() {
    return Yup.object().shape({
            streetNumber: Yup.string().required("Numer jest wymagany"),
            postCode: Yup.string().required("Kod jest wymagany"),
            place: Yup.string().required("Miejscowość jest wymagana"),
            country: Yup.string().required("Wybierz kraj")
        }
    );
}

const SignupSchema = Yup.object().shape({
    taxId: Yup.mixed().when("individual", {is: false, then: Yup.string().required("Numer nip jest wymagany")}),
    address: Yup.object()
        .when("individual", {
            is: false,
            then: defaultAddressSchema()
        }),
    addressDelivery: Yup.object()
        .when("useAddressAsDelivery", {
            is: false,
            then: defaultAddressSchema()
        }).when("individual", {
            is: true,
            then: defaultAddressSchema()
        }),
});

function isTaxFreeDisabled(values: OrderProps) {
    return values.addressDelivery.country === "PL" || (values.address.country === "PL" && values.useAddressAsDelivery && !values.individual);
}

const AddressStepForm = ({onSuccess}: { initialValues: OrderProps, onSuccess: (a: OrderProps) => void }) => {
    const [taxPolandInfo, { open, close }] = useDisclosure(false);
    const [initialValues, setRecoilState] = useRecoilState<OrderProps>(deliveryFormAtom);
    if (window.Cypress) {
        window.orderForm = {
            setRecoilState
        };
        console.log("Mode cypress")
    }
    return (
        <div>
            <Formik
                validationSchema={SignupSchema}
                initialValues={initialValues}
                enableReinitialize
                onSubmit={(
                    values: OrderProps,
                    {setSubmitting}: FormikHelpers<OrderProps>
                ) => {
                    setTimeout(() => {
                        setSubmitting(false);
                        setRecoilState(a => ({...a, values}))
                        onSuccess(values);
                    }, 300);
                }}
            >
                {
                    ({values, touched, setFieldValue, errors}) => {

                        if (touched && isTaxFreeDisabled(values) && values.taxFree) {
                            setFieldValue("taxFree", false);
                        }
                        return <Form noValidate={true} name="delivery-form">
                            <Grid>

                                        <Grid.Col xs={4}>
                                            <Field name="fastDelivery" label="Szybka dostawa"
                                                   description="Dostawa tego samego dnia +10zł" as={CheckboxCard}/>
                                        </Grid.Col>
                                        <Grid.Col xs={4}>
                                            <Popover
                                                test-data={"tax-poland-info-popover"}
                                                opened={taxPolandInfo}
                                                withArrow
                                                position="bottom"
                                                closeOnEscape={false}
                                                width={260}
                                            ><Popover.Dropdown sx={{ pointerEvents: 'none' }}>
                                                <div style={{display: 'flex'}}>
                                                <Text size="sm">Niestety wybierając kraj dostawy Polska nie
                                                    możesz być zwolniony z podatku</Text>
                                                </div>
                                            </Popover.Dropdown>
                                            <Popover.Target>
                                                <div onMouseEnter={() => {
                                                    (isTaxFreeDisabled(values) && open())
                                                }} onMouseLeave={ close}>
                                                    <Field name="taxFree" label="Bez podatku VAT"
                                                           disabled={isTaxFreeDisabled(values)}
                                                           description="Zaznacz jeśli chcesz uzyskać zwolnienie z podatku VAT"
                                                           as={CheckboxCard}/>
                                                </div>

                                            </Popover.Target>
                                            </Popover>
                                </Grid.Col>
                                        <Grid.Col xs={4}>
                                            <Field name="individual" label="Osoba fizyczna"
                                                   description="Kontrahent jest osobą fizyczną" as={CheckboxCard}/>
                                        </Grid.Col>


                                {(!values.individual) && <div>
                                    <Grid.Col xs={12}>
                                        <Field
                                            required
                                            label="Nip"
                                            name={"taxId"}
                                            error="test"
                                            as={TextInputField}/>
                                    </Grid.Col>
                                    <Grid.Col xs={12}>
                                        <AddressForm namespace="address" title={"Adres na fakturze"}/>
                                    </Grid.Col>
                                    <Grid.Col xs={12}>
                                        <Field
                                            label="Użyj adresu jako adresu dostawy"
                                            name="useAddressAsDelivery" as={Checkbox}
                                            checked={values.useAddressAsDelivery}
                                        />
                                    </Grid.Col>

                                </div>}
                                {(!values.useAddressAsDelivery || values.individual) && <Grid.Col xs={12}>
                                    <AddressForm namespace="addressDelivery" title={"Adres dostawy"}/>
                                </Grid.Col>}

                            </Grid>
                            <Group position="right" mt="xl">
                                <Button type="submit">Dalej</Button>
                            </Group>

                            <JsonPrint data={values}/>
                            <JsonPrint data={errors}/>
                        </Form>;
                    }
                }
            </Formik>

        </div>
    );
};

export default AddressStepForm;
