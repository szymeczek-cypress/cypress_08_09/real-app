import {useField} from "formik";
import {SegmentedControl, SegmentedControlItem} from "@mantine/core";
import React from "react";

export const SegmentedControlField = ({
                                          name,
                                          data,
                                          ...rest
                                      }: { label: string, name: string, data: string[] | SegmentedControlItem[] }) => {
    const [, , helpers] = useField(name);
    const {setValue} = helpers;
    return (
        <SegmentedControl
            {...rest}
            onChange={(e: any) => {
                setValue(e)
            }}
            data={data}
        />
    );
};