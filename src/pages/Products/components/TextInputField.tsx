import {
    TextInput as MantineTextInput,
    TextInputProps,
} from "@mantine/core"

import {useField} from "formik";
import React from "react";

export const TextInputField = ({
                                   name,
                                   onBlur,
                                   onFocus,
                                   onChange,
                                   ...rest
                               }: { name: string } & Omit<TextInputProps, "value" | "error">) => {
    const [{ value, onBlur: formikOnBlur }, { error, touched }, { setValue, setTouched }] =
        useField(name)
    return (
        <MantineTextInput
            {...rest}
            name={name}
            error={touched && error}
            value={value}
            onBlur={(e) => {
                formikOnBlur(e)
                setTouched(true)
                onBlur && onBlur(e)
            }}
            onFocus={(e) => {
                onFocus && onFocus(e)
            }}
            onChange={(v) => {
                setValue(v.target.value)
                onChange && onChange(v)
            }}
        />
    )
}