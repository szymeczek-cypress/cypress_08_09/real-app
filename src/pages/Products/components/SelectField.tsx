import {Select, SelectProps} from "@mantine/core";
import {useField} from "formik";
import React from "react";

export const SelectField = ({
                                name,
                                onChange,
                                data,
                                ...rest
                            }: { label: string, name: string } & Omit<SelectProps, "value" | "error">) => {
    const [{value, onBlur: formikOnBlur, onChange: formikOnChange}, {error, touched}, {
        setValue,
        setTouched,
    }] =
        useField(name);
    return (
        <Select required
                {...rest}
                name={name}
                value={value}
                error={touched && error}
                onChange={(e: any) => {
                    setValue(e)
                    onChange && onChange(e);
                    formikOnChange(e)
                }}
                onBlur={(e) => {
                    formikOnBlur(e)
                    setTouched(true)
                }}
                data={data}/>
    );
};