import {NumberInput, NumberInputProps} from "@mantine/core";
import {useField} from "formik";
import React from "react";

export const NumberField = ({
                                name,
                                onChange,
                                ...rest
                            }: { label: string, name: string } & Omit<NumberInputProps, "value" | "error">) => {
    const [field, meta, helpers] = useField(name);
    const {setValue} = helpers;
    return (
        <NumberInput
            parser={(value: string) => value?.replace(",", ".").replace(/[^\d.]/g, '')}
            value={field.value}
            {...rest}
            onChange={(e: any) => {
                setValue(e)
                onChange && onChange(e)
            }}
        />
    );
};