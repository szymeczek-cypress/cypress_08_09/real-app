import React, {useState} from 'react';
import {Stepper} from "@mantine/core";
import AddressStepForm, {OrderProps} from "./components/AddressStepForm";
import {useRecoilState} from "recoil";
import {appModel, deliveryFormAtom} from "../../state/context";
import {useLocalStorage} from "@mantine/hooks";
import {PaymentsGrid} from "./components/PaymentsGrid";
import axios from "axios";

type Props = {
    productId: string,
}
export const BuyAndPayStepper = ({productId}: Props) => {
    const [deliveryForm, setDeliveryForm] = useRecoilState<OrderProps>(deliveryFormAtom);
    const [formValue, setFormValue] = useLocalStorage({
        key: 'form-buy-and-pay',
        defaultValue: {step: 0, deliveryForm: appModel().addressForm}
    });

    const [active, setActive] = useState(formValue.step);

    const nextStep = () => {
        setActive((current) => {
            return current < 3 ? current + 1 : 0
        })


    };
    return (
        <Stepper active={active} onStepClick={setActive} breakpoint="sm">
            <Stepper.Step label="Dostawa" description="Forma dostawy">
                <AddressStepForm initialValues={deliveryForm} onSuccess={(a) => {
                    setFormValue((c) => ({...c, deliveryForm: a, step: 1}))
                    setDeliveryForm(a)
                    nextStep();
                }}/>
            </Stepper.Step>
            <Stepper.Step label="Płatność" description="" disabled={formValue.step < 1}>
                <PaymentsGrid onSuccess={() => {
                    axios.post("/api/create-order", {...formValue.deliveryForm });
                    setFormValue((c) => ({...c, step: 0}))
                    nextStep()
                }}/>
            </Stepper.Step>
            <Stepper.Completed>
                Zamówienie (produkt: ${productId}) zostało złożone. Oczekuj dostawy
            </Stepper.Completed>
        </Stepper>
    )


};
