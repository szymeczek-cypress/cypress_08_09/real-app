import React from 'react';
import {Heart} from 'tabler-icons-react';
import {ActionIcon, Badge, Button, Card, createStyles, Group, Image, Text, useMantineTheme,} from '@mantine/core';
import {Link} from "react-router-dom";

const useStyles = createStyles((theme) => ({
    card: {
        backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white,
    },

    section: {
        borderBottom: `1px solid ${
            theme.colorScheme === 'dark' ? theme.colors.dark[4] : theme.colors.gray[3]
        }`,
        paddingLeft: theme.spacing.md,
        paddingRight: theme.spacing.md,
        paddingBottom: theme.spacing.md,
    },

    like: {
        color: theme.colors.red[6],
    },

    label: {
        textTransform: 'uppercase',
        fontSize: theme.fontSizes.xs,
        fontWeight: 700,
    },
}));

interface BadgeCardProps {
    id: string;
    image: string;
    title: string;
    category: string;
    description: string;
    badges: {
        emoji: string;
        label: string;
    }[];
}

export function ProductCard({id, image, title, description, category, badges}: BadgeCardProps) {
    const {classes} = useStyles();
    const theme = useMantineTheme();

    const features = badges.map((badge) => (
        <Badge
            color={theme.colorScheme === 'dark' ? 'dark' : 'gray'}
            key={badge.label}
            leftSection={badge.emoji}
        >
            {badge.label}
        </Badge>
    ));

    return (
        <Card withBorder radius="md" p="md" className={classes.card} data-cy="product-card"
              data-test-id={`product-${id}`} data-test-name={title}>
            <Card.Section>
                <Image src={image} alt={title} height={180}/>
            </Card.Section>

            <Card.Section className={classes.section} mt="md" style={{minHeight: 200}}>
                <Group position="apart">
                    <Text size="lg" weight={500}>
                        {title}
                    </Text>
                    <Badge size="sm">{category}</Badge>
                </Group>
                <Text size="sm" mt="xs">
                    {description}
                </Text>
            </Card.Section>

            <Card.Section className={classes.section} style={{minHeight: 150}}>
                <Text mt="md" className={classes.label} color="dimmed">
                    Właściwosci
                </Text>
                <Group spacing={7} mt={5}>
                    {features}
                </Group>
            </Card.Section>
            <Card.Section className={classes.section}>
                <Group spacing={30}>
                    <div>
                        <Text size="xl" weight={700} sx={{lineHeight: 1}}>
                            999.00 PLN
                        </Text>
                        <Text size="sm" color="dimmed" weight={500} sx={{lineHeight: 1}} mt={3}>
                            za tonę
                        </Text>
                    </div>
                </Group>
            </Card.Section>
            <Group mt="xs">
                <Button data-test={`buy-and-pay-${id}`} component={Link} to={`${id}/buy-and-pay`} radius="md"
                        style={{flex: 1}}>
                    Kupuję i płacę
                </Button>
                <ActionIcon variant="default" radius="md" size={36} data-test="like-button">
                    <Heart size={18} className={classes.like}/>
                </ActionIcon>
            </Group>
        </Card>
    );
}