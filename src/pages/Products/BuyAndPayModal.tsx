import React from 'react';
import {Modal} from '@mantine/core';
import {useRecoilValue, useSetRecoilState} from "recoil";
import {modalBuyAndPayOpened} from "../../state/context";
import {BuyAndPayStepper} from "./BuyAndPayStepper";
import {useParams} from "react-router-dom";

export const BuyAndPayModal = () => {
    const opened = useRecoilValue(modalBuyAndPayOpened);
    const setOpened = useSetRecoilState(modalBuyAndPayOpened);
    const {productId} = useParams();
    return (
        <>
            <Modal
                opened={opened}
                size="xl"
                onClose={() => {
                    setOpened(false)
                }}
                title="Kupuję i płacę"
            >{productId && <BuyAndPayStepper productId={productId}/>}
            </Modal>
        </>
    );
};

