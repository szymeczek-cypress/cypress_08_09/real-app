import {Box, Button, Group, List, TextInput, ThemeIcon} from "@mantine/core";
import {useForm} from "@mantine/form";
import React from "react";
import {useListState} from "@mantine/hooks";
import {IconCircleCheck} from "@tabler/icons-react";

export const RetryAbility = () => {
    const form = useForm({
        initialValues: {
            task: '',
        },

        validate: {},
    });

    const [values, handlers] = useListState(["Zadanie 1"]);
    return (
        <Box maw={300} mx="auto">
            <form onSubmit={form.onSubmit((values) => {
                setTimeout(() => handlers.append(values.task), 2000);
                form.reset()
            })}>
                <TextInput
                    withAsterisk
                    name="task-input"
                    label="Zadanie"
                    placeholder="Wpisz zadanie"
                    {...form.getInputProps('task')}
                />

                <Group position="right" mt="md">
                    <Button type="submit">Submit</Button>
                </Group>
            </form>
            <List
                spacing="xs"
                size="sm"
                data-test="tasks-list"
                center
                icon={
                    <ThemeIcon color="teal" size={24} radius="xl">
                        <IconCircleCheck size="1rem"/>
                    </ThemeIcon>
                }
            > {values.map(a =>
                <List.Item>{a}</List.Item>
            )}
            </List>
        </Box>
    );
};
