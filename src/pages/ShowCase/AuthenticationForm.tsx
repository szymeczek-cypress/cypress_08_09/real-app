import { useToggle, upperFirst } from '@mantine/hooks';
import { useForm } from '@mantine/form';
import {
    TextInput,
    PasswordInput,
    Text,
    Paper,
    Group,
    PaperProps,
    Button,
    Divider,
    Checkbox,
    Anchor,
    Stack,
} from '@mantine/core';


export function AuthenticationForm(props: PaperProps) {
    const [type, toggle] = useToggle(['login', 'register']);
    const form = useForm({
        initialValues: {
            email: '',
            name: '',
            password: '',
            terms: true,
        },

        validate: {
            email: (val) => (/^\S+@\S+$/.test(val) ? null : 'Niepoprawny email'),
            password: (val) => (val.length <= 6 ? 'Hasło musi zawierać więcej niż 6 znaków' : null),
        },
    });

    return (
        <Paper radius="md" p="xl" withBorder {...props} data-test="auth-box">
            <Text size="lg" weight={500}>
                Witaj na stronie, {type}
            </Text>

            <Divider label="" labelPosition="center" my="lg" />

            <form onSubmit={form.onSubmit(() => {})}>
                <Stack>
                    {type === 'register' && (
                        <TextInput
                            label="Imię"
                            placeholder="Imię"
                            value={form.values.name}
                            onChange={(event) => form.setFieldValue('name', event.currentTarget.value)}
                            radius="md"
                        />
                    )}

                    <TextInput
                        required
                        label="Email"
                        data-test="auth-email"
                        placeholder="hello@szymeczek.com"
                        value={form.values.email}
                        onChange={(event) => form.setFieldValue('email', event.currentTarget.value)}
                        error={form.errors.email && 'Invalid email'}
                        radius="md"
                    />

                    <PasswordInput
                        required
                        label="Hasło"
                        data-test="auth-password"
                        placeholder="Hasło"
                        value={form.values.password}
                        onChange={(event) => form.setFieldValue('password', event.currentTarget.value)}
                        error={form.errors.password && 'Hasło musi zawierać więcej niż 6 znaków'}
                        radius="md"
                    />

                    {type === 'register' && (
                        <Checkbox
                            label="Akceptuję regulamin"
                            checked={form.values.terms}
                            onChange={(event) => form.setFieldValue('terms', event.currentTarget.checked)}
                        />
                    )}
                </Stack>

                <Group position="apart" mt="xl">
                    <Anchor
                        component="button"
                        type="button"
                        color="dimmed"
                        onClick={() => toggle()}
                        size="xs"
                    >
                        {type === 'register'
                            ? 'Czy masz już konto? Zaloguj się.'
                            : "Nie masz jeszcze konta? Rejestracja"}
                    </Anchor>
                    <Button type="submit" radius="xl" data-test="auth-submit">
                        {upperFirst(type)}
                    </Button>
                </Group>
            </form>
        </Paper>
    );
}