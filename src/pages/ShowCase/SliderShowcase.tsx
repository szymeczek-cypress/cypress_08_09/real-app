import React, {useState} from 'react';
import {Card, createStyles, Slider} from "@mantine/core";

const useStyles = createStyles((theme) => ({
    card: {
        backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white,
    },
}));
export const SliderShowcase = (props: {}) => {
    const {classes} = useStyles();
    const [sliderValue, setSliderValue] = useState<number>(0);
    // Expose the setValue() method so that Cypress can set the app state

    if (window.Cypress) {
        window.app = {
            setSliderValue
        };
        console.log("Mode cypress")
    }

    return <Card withBorder radius="md" p="xl" className={classes.card}>
        <Slider
            value={sliderValue}
            defaultValue={sliderValue}
            className="sliderBorrow"
            onChange={(value) => setSliderValue(value)}
            data-cy={"input-slider"}

            marks={[
                {value: 20, label: '20%'},
                {value: 50, label: '50%'},
                {value: 80, label: '80%'},
            ]}
        />
    </Card>
};
