import { useDisclosure } from '@mantine/hooks';
import {LoadingOverlay, Button, Group, Box, createStyles} from '@mantine/core';
import {AuthenticationForm} from "./AuthenticationForm";
const useStyles = createStyles(() => ({
    wrapper: {
        position: 'relative',
        marginBottom: 30,
    },

}));
export function OverlayDemo() {
    const [visible, { toggle }] = useDisclosure(false);
    const { classes } = useStyles();
    // Note that position: relative is required
    return (
        <Box>
            <Group className={classes.wrapper} position="center">
                <Button data-test="toggle-overlay" onClick={toggle}>Toggle overlay</Button>
            </Group>
            <Box pos="relative">
                <LoadingOverlay visible={visible} overlayBlur={2} />
                <AuthenticationForm/>
            </Box>

        </Box>
    );
}