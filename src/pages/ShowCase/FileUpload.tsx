import {useRef, useState} from 'react';
import {Button, createStyles, Group, Text} from '@mantine/core';
import {Dropzone, MIME_TYPES} from '@mantine/dropzone';
import {IconCloudUpload, IconDownload, IconX} from '@tabler/icons-react';

const useStyles = createStyles((theme) => ({
    wrapper: {
        position: 'relative',
        marginBottom: 30,
    },

    dropzone: {
        borderWidth: 1,
        paddingBottom: 50,
    },

    icon: {
        color: theme.colorScheme === 'dark' ? theme.colors.dark[3] : theme.colors.gray[4],
    },

    control: {
        bottom: -20,
    },
}));

export function DropzoneButton() {
    const {classes, theme} = useStyles();
    const openRef = useRef<() => void>(null);
    const [file, setFile] = useState<File[]>();

    return (
        <div className={classes.wrapper}>

            <Dropzone
                data-test="file-upload"
                openRef={openRef}
                onDrop={a => {
                    setFile(a)
                }}
                className={classes.dropzone}
                radius="md"
                accept={[MIME_TYPES.pdf]}
                maxSize={30 * 1024 ** 2}
            >
                <div style={{pointerEvents: 'none'}}>

                    <Group position="center">
                        <Dropzone.Accept>
                            <IconDownload
                                size={50}
                                color={theme.colors[theme.primaryColor][6]}
                                stroke={1.5}
                            />
                        </Dropzone.Accept>
                        <Dropzone.Reject>
                            <IconX size={50} color={theme.colors.red[6]} stroke={1.5}/>
                        </Dropzone.Reject>
                        <Dropzone.Idle>
                            <IconCloudUpload
                                size={50}
                                color={theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.black}
                                stroke={1.5}
                            />
                        </Dropzone.Idle>
                    </Group>

                    <Text ta="center" fw={700} fz="lg" mt="xl">
                        <Dropzone.Accept>Przeciągnij lub upuść plik tutaj</Dropzone.Accept>
                        <Dropzone.Reject>Plik pdf musi mieć mienij niż  30mb</Dropzone.Reject>

                        <Dropzone.Idle>Upload resume</Dropzone.Idle>
                    </Text>
                    <Text ta="center" fz="sm" mt="xs" c="dimmed">
                        Przeciągnij lub upuść plik tutaj. Akceptujemy tylko pliki <i>.pdf</i> mniejsze niż 30mb.
                    </Text>
                    {file?.length && <Text ta="center" fz="sm" mt="xs" c="dimmed" data-test="file-result">Wgrano: {file?.map(a => a.name)}</Text>}
                </div>
            </Dropzone>
        <Group position="center">
            <Button className={classes.control} size="xs" radius="xl" onClick={() => openRef.current?.()}>
                Select files
            </Button>
        </Group>
        </div>
    );
}