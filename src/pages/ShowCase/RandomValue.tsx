import React, {useEffect, useState} from 'react';
import {Card, createStyles} from "@mantine/core";

const useStyles = createStyles((theme) => ({
    card: {
        backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white,
    },
}));
export const RandomValue = (props: {}) => {
    const {classes} = useStyles();
    const [value, setValue] = useState<string>("🎁");

    useEffect(() => {
        setTimeout(() => {
            setValue((Math.floor(Math.random() * 10 + 1)) + "")
        }, 1500)
    }, [])

    return <Card withBorder radius="md" p="xl" className={classes.card}>
        <div className="random-number-example">
            Random number: <span id="random-number">{value}</span>
        </div>
    </Card>
};
