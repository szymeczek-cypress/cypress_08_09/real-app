import React from "react";
import {Container, Grid, SimpleGrid, Space} from '@mantine/core';
import {DropzoneButton} from "./FileUpload";
import {OverlayDemo} from "./OverlayDemo";
import {SliderShowcase} from "./SliderShowcase";
import {CourseItem} from "./CourseItem";


export function ShowCaseGrid() {
    return (
        <Container my="md">
            <SimpleGrid cols={2} spacing="md" breakpoints={[{maxWidth: 'sm', cols: 1}]}>
                <CourseItem {...{
                    "image": "https://images.pexels.com/photos/2004161/pexels-photo-2004161.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
                    "link": "#/show-case/simple-form",
                    "title": "Prosty formularz",
                    "rating": "Łatwe",
                    "description": "",
                    "author": {
                        "name": "",
                        "image": ""
                    }
                }}/>
                <CourseItem {...{
                    "image": "https://images.pexels.com/photos/2004161/pexels-photo-2004161.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
                    "link": "#/show-case/overlay",
                    "title": "Widoczność elementu",
                    "rating": "Średnie",
                    "description": "",
                    "author": {
                        "name": "",
                        "image": ""
                    }
                }}/>
                <CourseItem {...{
                    "image": "https://images.pexels.com/photos/2004161/pexels-photo-2004161.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
                    "link": "#/show-case/drag-and-drop",
                    "title": "Drag and drop",
                    "rating": "Łatwe",
                    "description": "",
                    "author": {
                        "name": "",
                        "image": ""
                    }
                }}/>

                <CourseItem {...{
                    "image": "https://images.pexels.com/photos/2004161/pexels-photo-2004161.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
                    "link": "#/show-case/random",
                    "title": "Random",
                    "rating": "Średnie",
                    "description": "",
                    "author": {
                        "name": "",
                        "image": ""
                    }
                }}/>
                <CourseItem {...{
                    "image": "https://images.pexels.com/photos/2004161/pexels-photo-2004161.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
                    "link": "#/show-case/dropzone",
                    "title": "Upload",
                    "rating": "Łatwe",
                    "description": "",
                    "author": {
                        "name": "",
                        "image": ""
                    }
                }}/>

                <CourseItem {...{
                    "image": "https://images.pexels.com/photos/2004161/pexels-photo-2004161.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
                    "link": "#/products",
                    "title": "Lista elementów",
                    "rating": "Łatwe",
                    "description": "",
                    "author": {
                        "name": "",
                        "image": ""
                    }
                }}/>

            </SimpleGrid>
            <Space h="md"/>
            <Grid gutter="md">
                <Grid.Col md={3} sm={12}>
                    <Space h="md"/>
                    <SliderShowcase/>
                </Grid.Col>
            </Grid>


        </Container>
    );
}