import {Box, Button, Group, List, TextInput, ThemeIcon} from "@mantine/core";
import {useForm} from "@mantine/form";
import React from "react";
import {useListState} from "@mantine/hooks";
import {IconCircleCheck} from "@tabler/icons-react";
import {SwitchesCard} from "./ShowCase/Switch";

export const SwitchesPage = () => {

    return (
        <SwitchesCard {...{
            "title": "Configure notifications",
            "description": "Choose what notifications you want to receive",
            "data": [
                {
                    "title": "Messages",
                    "description": "Direct messages you have received from other users"
                },
                {
                    "title": "Review requests",
                    "description": "Code review requests from your team members"
                },
                {
                    "title": "Comments",
                    "description": "Daily digest with comments on your posts"
                },
                {
                    "title": "Recommendations",
                    "description": "Digest with best community posts from previous week"
                }
            ]
        }}/>
    );
};
