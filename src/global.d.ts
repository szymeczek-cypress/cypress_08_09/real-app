import {Cypress} from 'cypress';

declare global {
    interface Window {
        Cypress: Cypress,
        app: {
            setSliderValue(value: number): any
        }
        orderForm: {
            setRecoilState(value: any): any
        }
    }
}