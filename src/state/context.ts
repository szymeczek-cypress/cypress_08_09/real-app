import {atom, selector} from "recoil";
import {OrderProps} from "../pages/Products/components/AddressStepForm";

export interface FileDTO {
    /** @format int64 */
    id?: number;
    context?: string;
    name?: string;
    type?: string;
    createdBy?: string;
    modifiedBy?: string;
    /** @format date-time */
    createdDate?: string;
    /** @format date-time */
    modifiedDate?: string;
    content?: string[];
    contentContentType?: string;
}

export interface PartyDTO {
    /** @format int64 */
    id?: number;
    context?: string;
    taxId?: string;
    name?: string;
    address?: string;
    logotype?: FileDTO;
}

export interface DocumentDraftDTO {
    /** @format int64 */
    id?: number;
    context: string;
    /** @format date-time */
    createdDate?: string;
    createdBy?: string;
    status: "NEW" | "ARCHIVED";
    documentFormat: "PDF" | "IMAGE";
    documentDraftType: "INVOICE";
    file?: FileDTO;
}
export const appModel = () => {
    const addressForm:OrderProps = {
        address: {
            type: "STREET",
            street: "",
            streetNumber: "",
            flatNumber: "",
            postCode: "",
            place: "",
            country: ""
        },
        addressDelivery: {
            type: "STREET",
            street: "",
            streetNumber: "",
            flatNumber: "",
            postCode: "",
            place: "",
            country: ""
        },
        useAddressAsDelivery: true,
        phone: "",
        fastDelivery: true,
        taxFree: true,
        individual: false,
        taxId: "",
    }

    return {addressForm}
};
export const model = appModel();

export const deliveryFormAtom = atom<OrderProps>({
    key: 'DeliveryFrom',
    default: {...model.addressForm},
});
export const modalBuyAndPayOpened = atom({
    key: 'ModalBuyAndPayOpened',
    default: false,
});

export const draverPartyValue = atom({
    key: 'DraverPartyValue',
    default: {},
});


export const documentsDraftsQuery = selector({
    key: 'DocumentDrafts',
    get: async ({get}): Promise<any> => {
        return Promise;
    },
});
export const documentsDraftsState = atom<DocumentDraftDTO[]>({
    key: 'DocumentsDrafts',
    default: documentsDraftsQuery,
});

// @ts-ignore
if (window.Cypress) {
    // @ts-ignore
    window.model = model
}