import React from 'react';
import {useLocalStorage} from "@mantine/hooks";

interface ImStatelessProps {
    data: any;
}

export const JsonPrint = React.memo<ImStatelessProps>((props: ImStatelessProps) => {
    const [value] = useLocalStorage({key: 'developer-mode', defaultValue: 'false'});
    return (
        <>
            {process.env.NODE_ENV !== 'production'  && value === "true" && (
                <pre>
        {
            JSON.stringify(props.data, null, 2)
        }
      </pre>
            )}
        </>
    )
});
