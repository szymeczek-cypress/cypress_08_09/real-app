import React, {Suspense} from 'react';
import './App.css';
import {Routes} from './routes/Routes';
import {MantineProvider} from "@mantine/core";
import {RecoilRoot} from "recoil";

import {HashRouter} from "react-router-dom";


function App() {
    return (
        <Suspense fallback={null}>
            <HashRouter>
                <RecoilRoot>
                    <MantineProvider withGlobalStyles withNormalizeCSS>
                        <Routes/>
                    </MantineProvider>
                </RecoilRoot>
            </HashRouter>
        </Suspense>
    );
}

export default App;
