import React from 'react';
import {data} from "./data";
import {AppFooter} from "./components/AppFooter";
import {AppShell} from "@mantine/core";
import {Outlet} from "react-router-dom";
import {HeaderMenuColored} from "./components/HeaderMenuColored";

export const MainLayout = () => {
    return (
        <AppShell
            header={<HeaderMenuColored links={data}/>}
            styles={(theme) => ({
                main: {backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.gray[0]},
            })}
            footer={<AppFooter data={data}/>}
        >

            <React.Suspense fallback={<div>Loading...</div>}>
                <Outlet/>
            </React.Suspense>

        </AppShell>

    );
};

