import React, {useEffect, useState} from 'react';
import {Code, createStyles, Group, Navbar, ScrollArea} from '@mantine/core';
import {Adjustments, Gauge, Lock, Notes,} from 'tabler-icons-react';
import {UserButton} from './UserButton';
import {LinksGroup} from './NavbarLinksGroup';
import {Logo} from './Logo';
import {User} from "./types/User";
import {userDefault} from "../data";

const mockdata = [
    { label: 'Dashboard', icon: Gauge, link: "/dashboard" },
    {    label: 'Dokumenty',
        icon: Notes,
        initiallyOpened: true,
        links: [
            { label: 'Nowe', link: '/documents-draft' },
        ],
    },/*
    {
        label: 'Releases',
        icon: CalendarStats,
        links: [
            { label: 'Upcoming releases', link: '/' },
            { label: 'Previous releases', link: '/' },
            { label: 'Releases schedule', link: '/' },
        ],
    },
    { label: 'Analytics', icon: PresentationAnalytics },
    { label: 'Contracts', icon: FileAnalytics },*/
    { label: 'Settings', icon: Adjustments },
    {
        label: 'Security',
        icon: Lock,
        links: [
            { label: 'Enable 2FA', link: '/' },
            { label: 'Change password', link: '/' },
            { label: 'Recovery codes', link: '/' },
        ],
    },
];

const useStyles = createStyles((theme) => ({
    navbar: {
        backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.white,
        paddingBottom: 0,
    },

    header: {
        padding: theme.spacing.md,
        paddingTop: 0,
        marginLeft: -theme.spacing.md,
        marginRight: -theme.spacing.md,
        color: theme.colorScheme === 'dark' ? theme.white : theme.black,
        borderBottom: `1px solid ${
            theme.colorScheme === 'dark' ? theme.colors.dark[4] : theme.colors.gray[3]
        }`,
    },

    links: {
        marginLeft: -theme.spacing.md,
        marginRight: -theme.spacing.md,
    },

    linksInner: {
        paddingTop: theme.spacing.xl,
        paddingBottom: theme.spacing.xl,
    },

    footer: {
        marginLeft: -theme.spacing.md,
        marginRight: -theme.spacing.md,
        borderTop: `1px solid ${
            theme.colorScheme === 'dark' ? theme.colors.dark[4] : theme.colors.gray[3]
        }`,
    },
}));

export function AppNavbar() {
    const { classes } = useStyles();
    const links = mockdata.map((item) => <LinksGroup {...item} key={item.label} />);
    const [user, setUser] = useState<User| null>(null);
    return (
        <Navbar width={{ sm: 300 }} p="md" className={classes.navbar}>
            <Navbar.Section className={classes.header}>
                <Group position="apart">
                    <Logo />
                    <Code sx={{ fontWeight: 700 }}>v1.0.0-aplha1</Code>
                </Group>
            </Navbar.Section>

            <Navbar.Section grow className={classes.links} component={ScrollArea}>
                <div className={classes.linksInner}>{links}</div>
            </Navbar.Section>

            {user && <Navbar.Section className={classes.footer}>
                <UserButton
                    name={user.name}
                    subName={user.jobTitle}
                    image={user.avatar}
                />
            </Navbar.Section>}
        </Navbar>
    );
}