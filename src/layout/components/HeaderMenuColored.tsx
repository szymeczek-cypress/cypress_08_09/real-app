import React from 'react';
import {Burger, Center, Container, createStyles, Group, Header, Menu, Paper, Text, Transition} from '@mantine/core';
import { useToggle} from '@mantine/hooks';
import {ChevronDown} from 'tabler-icons-react';
import {Link} from "react-router-dom";

const HEADER_HEIGHT = 56;
const useStyles = createStyles((theme) => ({
    header: {
        backgroundColor: theme.colors[theme.primaryColor][6],
        borderBottom: 0,
    },

    inner: {
        height: HEADER_HEIGHT,
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },

    burger: {
        [theme.fn.largerThan('sm')]: {
            display: 'none',
        },
    },

    link: {
        display: 'block',
        lineHeight: 1,
        padding: '8px 12px',
        borderRadius: theme.radius.sm,
        textDecoration: 'none',
        color: theme.white,
        fontSize: theme.fontSizes.sm,
        fontWeight: 500,

        '&:hover': {
            backgroundColor: theme.colors[theme.primaryColor][theme.colorScheme === 'dark' ? 7 : 5],
        },
    },
    dropdown: {
        position: 'absolute',
        top: HEADER_HEIGHT,
        left: 0,
        right: 0,
        zIndex: 0,
        borderTopRightRadius: 0,
        borderTopLeftRadius: 0,
        borderTopWidth: 0,
        overflow: 'hidden',

        [theme.fn.largerThan('sm')]: {
            display: 'none',
        },
    },
    linkLabel: {
        marginRight: 5,
    },
}));
type LinkType = { link?: string; label: string; links?: { id: number, link: string; label: string }[] }

type HeaderMenuColoredProps = {
    links: LinkType[];
}

export function HeaderMenuColored({links}: HeaderMenuColoredProps) {
    const [opened, toggleOpened] = useToggle();
    const {classes} = useStyles();
    const items = links.map((link) => {
        const menuItems = link.links?.map((item) => (
            <Menu.Item<typeof Link> component={Link} to={item.link || "#"} key={item.id}>{item.label}</Menu.Item>
        ));

        if (menuItems) {
            return (
                <Menu
                    key={link.label}
                    trigger="hover"
                >
                    <Menu.Target>
                        <Text<typeof Link> component={Link} to={link.link || "#"}
                                           className={classes.link}
                        >
                            <Center>
                                <span className={classes.linkLabel}>{link.label}</span>
                                <ChevronDown size={12}/>
                            </Center>
                        </Text>
                    </Menu.Target>
                    {menuItems}
                </Menu>
            );
        }

        return (
            <div key={link.label}>
                {link.link &&
                    <Text<typeof Link> component={Link} to={link.link} className={classes.link}>{link.label}</Text>}
                {!link.link && <Text>{link.label}</Text>}

            </div>
        );
    });

    return (
        <Header height={56} className={classes.header}>
            <Container>
                <div className={classes.inner}>
                    <div/>
                    <Group spacing={5}>
                        {items}
                    </Group>
                    <Burger
                        opened={opened}
                        onClick={() => toggleOpened()}
                        className={classes.burger}
                        size="sm"
                        color="#fff"
                    />
                    <Transition transition="pop-top-right" duration={200} mounted={opened}>
                        {(styles) => (
                            <Paper className={classes.dropdown} withBorder style={styles}>
                                {items}
                            </Paper>
                        )}
                    </Transition>
                </div>

            </Container>

        </Header>
    );
}