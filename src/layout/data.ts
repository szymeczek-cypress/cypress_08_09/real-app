import {User} from "./components/types/User";

export const
    userLinks = [
        {
            "link": "#",
            "label": "Privacy & Security"
        },
        {
            "link": "#",
            "label": "Account settings"
        },
        {
            "link": "#",
            "label": "Support options"
        }
    ];
export const
    mainLinks = [
        {
            "link": "#",
            "label": "Book a demo"
        },
        {
            "link": "#",
            "label": "Documentation"
        },
        {
            "link": "#",
            "label": "Community"
        },
        {
            "link": "#",
            "label": "Academy"
        },
        {
            "link": "#",
            "label": "Forums"
        }
    ]

export const data =  [
    {id: 1, "label": "Strona główna", link: "/"},
    {
        id:2,
        "label": "Produkty",
        "link": "/products"
    }
]
export const userDefault: User = {
    avatar: 'https://images.unsplash.com/photo-1508214751196-bcfd4ca60f91?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=255&q=80',
    jobTitle: 'Mistrz ',
    name: 'Oszczędny Wymiatacz'
};